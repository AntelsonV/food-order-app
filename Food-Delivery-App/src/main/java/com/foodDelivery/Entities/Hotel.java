package com.foodDelivery.Entities;

import java.time.LocalDate;
import java.util.Arrays;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;

@Document(collection = "HotelDetails")
public class Hotel {
	@Id
    private int id;
	private String name;
	private String address;
	@JsonFormat(pattern = "YYYY/MM/DD")
	private LocalDate  date;
    private String filename;
    private String contentType;
    private byte[] content;
    private double rating;
    
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public Hotel(int id, String name, String address, LocalDate date, String filename, String contentType,
			byte[] content, double rating) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.date = date;
		this.filename = filename;
		this.contentType = contentType;
		this.content = content;
		this.rating = rating;
	}
	public Hotel() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Hotel [id=" + id + ", name=" + name + ", address=" + address + ", date=" + date + ", filename="
				+ filename + ", contentType=" + contentType + ", content=" + Arrays.toString(content) + ", rating="
				+ rating + "]";
	}
	
	

}
