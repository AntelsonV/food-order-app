package com.foodDelivery.Entities;

import java.time.LocalDate;
import java.util.Arrays;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@Document (collection = "DishDetails")

public class Dish {
    @Id
    private int id;
    private int hotelid;
    private String name;
    private int price;
    private byte[] image;
    private LocalDate date;
    private double rating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getHotelid() {
        return hotelid;
    }

    public void setHotelid(int hotelid) {
        this.hotelid = hotelid;
    }

    public Dish(int id, int hotelid, String name, int price, byte[] image, LocalDate date, double rating) {
        this.id = id;
        this.hotelid = hotelid;
        this.name = name;
        this.price = price;
        this.image = image;
        this.date = date;
        this.rating = rating;
    }

    public Dish() {
    }

    @Override
    public String toString() {
        return "Dish [id=" + id + ", hotelid=" + hotelid + ", name=" + name + ", price=" + price + ", image="
                + Arrays.toString(image) + ", date=" + date + ", rating=" + rating + "]";
    }

    
}
