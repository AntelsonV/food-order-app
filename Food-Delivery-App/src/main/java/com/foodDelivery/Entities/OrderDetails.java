package com.foodDelivery.Entities;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;

@Document
public class OrderDetails {
	@Id
	private int id;
	private int userid;
	private int dishid;
	private int hotelid;
	private int initialprice;
	private int quantity;
	private int itemtotal;
	private int deliveryfee;
	private float gst;
	private float totalamount;
	private String address;
	@JsonFormat(pattern = "YYYY/MM/DD")
	private LocalDate date;
	private String status;

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDishid() {
		return dishid;
	}

	public void setDishid(int dishid) {
		this.dishid = dishid;
	}

	public int getHotelid() {
		return hotelid;
	}

	public void setHotelid(int hotelid) {
		this.hotelid = hotelid;
	}

	public int getInitialprice() {
		return initialprice;
	}

	public void setInitialprice(int initialprice) {
		this.initialprice = initialprice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getItemtotal() {
		return itemtotal;
	}

	public void setItemtotal(int itemtotal) {
		this.itemtotal = itemtotal;
	}

	public int getDeliveryfee() {
		return deliveryfee;
	}

	public void setDeliveryfee(int deliveryfee) {
		this.deliveryfee = deliveryfee;
	}

	public float getGst() {
		return gst;
	}

	public void setGst(float gst) {
		this.gst = gst;
	}

	public float getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(float totalamount) {
		this.totalamount = totalamount;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public OrderDetails(int id, int userid, int dishid, int hotelid, int initialprice, int quantity, int itemtotal,
			int deliveryfee, float gst, float totalamount, String address, LocalDate date, String status) {
		this.id = id;
		this.userid = userid;
		this.dishid = dishid;
		this.hotelid = hotelid;
		this.initialprice = initialprice;
		this.quantity = quantity;
		this.itemtotal = itemtotal;
		this.deliveryfee = deliveryfee;
		this.gst = gst;
		this.totalamount = totalamount;
		this.address = address;
		this.date = date;
		this.status = status;
	}

	public OrderDetails() {
	}

	@Override
	public String toString() {
		return "OrderDetails [id=" + id + ", userid=" + userid + ", dishid=" + dishid + ", hotelid=" + hotelid
				+ ", initialprice=" + initialprice + ", quantity=" + quantity + ", itemtotal=" + itemtotal
				+ ", deliveryfee=" + deliveryfee + ", gst=" + gst + ", totalamount=" + totalamount + ", address="
				+ address + ", date=" + date + ", status=" + status + "]";
	}

}
