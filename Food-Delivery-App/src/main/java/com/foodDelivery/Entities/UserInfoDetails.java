//package com.foodDelivery.Entities;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//
//public class UserInfoDetails implements UserDetails {
//
//	User user;
//
//	public UserInfoDetails(User user) {
//		this.user = user;
//	}
//
//	@Override
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		List<GrantedAuthority> authorities = new ArrayList<>();
//		GrantedAuthority authority = new GrantedAuthority() {
//
//			@Override
//			public String getAuthority() {
//
//				return user.getRole();
//			}
//		};
//		authorities.add(authority);
//		return authorities;
//	}
//
//	@Override
//	public String getPassword() {
//		return user.getPassword();
//	}
//
//	@Override
//	public String getUsername() {
//		return user.getUsername();
//	}
//
//}
