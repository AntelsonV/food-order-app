package com.foodDelivery.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.foodDelivery.Entities.Dish;

@Repository
public interface DishRepository extends MongoRepository<Dish, Integer> {
    public Dish findById(int id);
}
