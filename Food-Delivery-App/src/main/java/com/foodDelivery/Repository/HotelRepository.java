package com.foodDelivery.Repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.foodDelivery.Entities.Hotel;

@Repository
public interface HotelRepository extends MongoRepository<Hotel, Integer>{
	
}
