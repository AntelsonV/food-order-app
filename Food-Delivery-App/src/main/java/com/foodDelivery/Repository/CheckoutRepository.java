package com.foodDelivery.Repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.foodDelivery.Entities.OrderDetails;

@Repository
public interface CheckoutRepository extends MongoRepository<OrderDetails, Integer> {


	 OrderDetails findById(int id);


    public List<OrderDetails> findByStatus(String status);

}
