package com.foodDelivery.Repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


import com.foodDelivery.Entities.User;
import java.util.List;


@Repository
public interface UserRepository extends MongoRepository<User, Integer> {

	public User findById(int id);
//	User findByUsername(String username);
	
}