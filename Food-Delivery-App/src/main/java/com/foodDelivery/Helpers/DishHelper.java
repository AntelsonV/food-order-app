package com.foodDelivery.Helpers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class DishHelper {

    public final String UPLOAD_DIR = new ClassPathResource("static/images/").getFile().getAbsolutePath();

    public DishHelper() throws IOException {
    }

    public boolean fileUploadHelper(MultipartFile file, byte[] data) {
        boolean f = false;
        try {
            System.out.println(UPLOAD_DIR);
            FileOutputStream fileOutputStream = new FileOutputStream(
                    UPLOAD_DIR + File.separator + file.getOriginalFilename());
            System.out.println(UPLOAD_DIR + file.getOriginalFilename());
            fileOutputStream.write(data);
            fileOutputStream.flush();
            fileOutputStream.close();
            f = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return f;

    }

}
