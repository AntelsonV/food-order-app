package com.foodDelivery.Email;

import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@Document(collection ="OTP")
public class OTPVerification {
	@Id
	private int id;
	private String email;
	private int otp;
	private LocalDateTime time;
	private boolean flag;

	@Override
	public String toString() {
		return "OTPVerification [id=" + id + ", email=" + email + ", otp=" + otp + ", time=" + time + ", flag=" + flag
				+ "]";
	}

	public OTPVerification() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OTPVerification(int id, String email, int otp, LocalDateTime time, boolean flag) {
		super();
		this.id = id;
		this.email = email;
		this.otp = otp;
		this.time = time;
		this.flag = flag;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getOtp() {
		return otp;
	}

	public void setOtp(int otp) {
		this.otp = otp;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

}