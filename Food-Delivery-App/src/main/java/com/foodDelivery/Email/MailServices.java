package com.foodDelivery.Email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServices {
@Autowired
private JavaMailSender javaMailSender;
@Autowired
OtpRepository otpRepository;
public void  sendotp(String email,int otp)
{
	SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
	simpleMailMessage.setFrom("aginamonica06@gmail.com");
	simpleMailMessage.setTo(email);
	simpleMailMessage.setSubject("Your otp code");
	simpleMailMessage.setText("Your otp code is:"+otp+".this otp is valid upto 10mins");
	javaMailSender.send(simpleMailMessage);
}
}
