package com.foodDelivery.Email;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OtpService {

	@Autowired
	OtpRepository otpRepository;

	public OTPVerification saveOtp(OTPVerification otpVerification) {
		return otpRepository.save(otpVerification);

	}

	public OTPVerification updateOtp(OTPVerification otpVerification) {
		return otpRepository.save(otpVerification);

	}

	public void deleteOtp(OTPVerification otpVerification) {
		otpRepository.delete(otpVerification);

	}
	public List<OTPVerification> getAllOtp() {
		return otpRepository.findAll();
	}
}
