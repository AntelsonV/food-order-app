package com.foodDelivery.Email;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import javax.annotation.processing.SupportedSourceVersion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.foodDelivery.Entities.User;
import com.foodDelivery.Services.UserServices;

@RestController
@RequestMapping("/otp")
@CrossOrigin("*")
public class MailController {
	@Autowired
	MailServices mailServices;

	@Autowired
	OtpService otpService;

	@Autowired
	UserServices userServices;

	@PostMapping
	@CrossOrigin("*")
	public String sendotp(@RequestBody OTPVerification otpVerification1) {
		System.out.println(otpVerification1.getEmail());
		int otp = GenerateOtp.generateOtp();
		mailServices.sendotp(otpVerification1.getEmail(), otp);
		OTPVerification otpVerification = new OTPVerification();
		int id = otpService.getAllOtp().size() + 1;
		otpVerification.setId(id);
		otpVerification.setEmail(otpVerification1.getEmail());
		otpVerification.setTime(LocalDateTime.now());
		otpVerification.setOtp(otp);
		otpVerification.setFlag(false);
		otpService.saveOtp(otpVerification);

		return otpVerification1.getEmail();
	}

	@PostMapping("/a/{otp}")
	public ResponseEntity<String> verifyotp(@RequestBody User user,@PathVariable("otp") String otp1) {
		int otp=Integer.parseInt(otp1);
		System.out.println(otp);
		System.out.println(user);
		List<OTPVerification> otpVerifications = otpService.getAllOtp();
		System.out.println(otpVerifications);
		for (OTPVerification otpVerification : otpVerifications) {
			if (user.getEmail().equals(otpVerification.getEmail()) && otp == otpVerification.getOtp()) {
				System.out.println("Otp Verified Successfully");
				if (LocalDateTime.now().getDayOfYear() == otpVerification.getTime().getDayOfYear()
						&& LocalDateTime.now().getDayOfMonth() == otpVerification.getTime().getDayOfMonth()
						&& LocalDateTime.now().getDayOfWeek() == otpVerification.getTime().getDayOfWeek()
						&& LocalDateTime.now().getHour() == otpVerification.getTime().getHour()) {
					if(LocalDateTime.now().getMinute()-otpVerification.getTime().getMinute()<=10) {						
						userServices.saveUser(user);
						return ResponseEntity.ok("Otp Verified Successfully");
					}
				}
				return ResponseEntity.status(HttpStatus.EARLY_HINTS).body("OTP Expired");

			}
		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("invalid otp");
	}
}
