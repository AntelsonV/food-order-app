package com.foodDelivery.Controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.foodDelivery.Entities.Dish;
import com.foodDelivery.Services.DishServices;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("dish")
@CrossOrigin("*")
public class DishController {
    @Autowired
    private DishServices dishServices;


    @PostMapping
    public Dish saveDish(@ModelAttribute("dish") Dish dish, @RequestParam("file") MultipartFile file) throws IOException{
        return dishServices.saveDish(file,dish);
    }

    @GetMapping
    public List<Dish> getDishies() {
        return dishServices.getDishies();
    }
  
    @GetMapping("/{id}")
    public Dish getDish(@PathVariable("id") int id) {
        return dishServices.getDish(id);
    }

    @PutMapping
    public Dish updateDish(@ModelAttribute("dish") Dish dish) {
        return dishServices.updateDish(dish);
    }

    @DeleteMapping("/{id}")
    public String deleteDish(@PathVariable("id") int id) {
        dishServices.deleteDish(id);
        return "Dish Removed Successfully";
    }

}
