package com.foodDelivery.Controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foodDelivery.Entities.OrderDetails;
import com.foodDelivery.Services.CheckoutService;

@RestController
@RequestMapping("/checkout")
@CrossOrigin("*")
public class CheckoutController {
	@Autowired
	private CheckoutService checkoutService;

	@PostMapping
	public OrderDetails hoteldetails(@RequestBody OrderDetails checkOut) {
		System.out.println(checkOut);
		int id=checkoutService.getAllOrder().size()+1;
		checkOut.setId(id);
		checkOut.setDate(LocalDate.now());
		// checkOut.setStatus("Pending");
		return checkoutService.saveOrder(checkOut);
	}
	
	@GetMapping
	public List<OrderDetails> getData() {
		return checkoutService.getAllOrder();
	}
	@PostMapping("/updateaddress")
	@CrossOrigin("*")
	public OrderDetails updateAddress(@RequestBody OrderDetails orderDetails) {
		System.out.println(orderDetails);
		return checkoutService.updateAddress(orderDetails);
	}

	@GetMapping("/status")
	public List<OrderDetails> getStatusPending(){
		return checkoutService.getStatusPending();
	}
}
