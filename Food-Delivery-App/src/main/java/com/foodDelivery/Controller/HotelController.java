package com.foodDelivery.Controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.foodDelivery.Entities.Hotel;
import com.foodDelivery.Entities.User;
import com.foodDelivery.Services.HotelService;

@RestController
@RequestMapping("/hotel")
public class HotelController {
	@Autowired
	HotelService hotelService;
	@PostMapping("/upload")
	@CrossOrigin("*")
	public Hotel uploadImage(@ModelAttribute("hotel") Hotel hotel, @RequestParam("file") MultipartFile file) {
//		try {
//			hotelService.saveImage(file);
//			return new ResponseEntity<>(hotel, HttpStatus.CREATED);
//		} catch (IOException e) {
//			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//		}
		try {
			byte[] data = file.getBytes();
			hotel.setDate(LocalDate.now());
			hotel.setContent(data);
			hotel.setFilename(file.getOriginalFilename());
			hotel.setContentType(file.getContentType());
			hotel.setRating(2.5);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hotelService.saveHotel(hotel);
	}

//	@GetMapping("/{id}")
//	public ResponseEntity<?> getHotel(@PathVariable int id) {
//		Hotel hotel = hotelService.getHotel(id);
//		if (hotel == null) {
//			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//		}
//
//		return ResponseEntity.ok()
//				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + hotel.getFilename() + "\"")
//				.header(HttpHeaders.CONTENT_TYPE, hotel.getContentType()).body(hotel.getContent());
//	}
	@GetMapping("/{id}")
	 @CrossOrigin("*")
	public Hotel getHotel(@PathVariable("id") int id) {
		return hotelService.getHotel(id);
	}

	@GetMapping
	 @CrossOrigin("*")
	public List<Hotel> getAllHotel() {
		return hotelService.getAllHotel();
	}

	@PutMapping("/update")
	 @CrossOrigin("*")
	public Hotel updateHotel(@RequestBody Hotel hotel) {
		hotel.setDate(LocalDate.now());
		return hotelService.updateHotel(hotel);
	}

	@DeleteMapping("/delete/{id}")
	 @CrossOrigin("*")
	public void deleteHotel(@PathVariable int id) {
		hotelService.deleteHotel(id);
	}
}
