package com.foodDelivery.Services;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.foodDelivery.Entities.Hotel;
import com.foodDelivery.Entities.User;
import com.foodDelivery.Repository.HotelRepository;

@Service
public class HotelService {
	@Autowired
	HotelRepository hotelRepository;

	public Hotel saveImage(MultipartFile file) throws IOException {
		Hotel hotel = new Hotel();
		hotel.setFilename(file.getOriginalFilename());
		hotel.setContentType(file.getContentType());
		hotel.setContent(file.getBytes());
		return hotelRepository.save(hotel);
	}

	public Hotel getHotel(int id) {
		return hotelRepository.findById(id).orElse(null);
	}

	public Hotel saveHotel(Hotel hotel) {
		return hotelRepository.save(hotel);
	}

	public List<Hotel> getAllHotel() {
		return hotelRepository.findAll();
	}

	public Hotel updateHotel(Hotel hotel) {
		
		return hotelRepository.save(hotel);
	}

	public void deleteHotel(int id) {
		hotelRepository.deleteById(id);
	}
}
