package com.foodDelivery.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.foodDelivery.Entities.OrderDetails;
import com.foodDelivery.Repository.CheckoutRepository;

@Component
public class CheckoutService {
	@Autowired
	private CheckoutRepository checkoutRepository;

	public OrderDetails saveOrder(OrderDetails checkout) {
		return checkoutRepository.save(checkout);
	}

	public List<OrderDetails> getAllOrder() {
		return checkoutRepository.findAll();
	}

	public OrderDetails getOrder(int id) {
		return checkoutRepository.findById(id);
	}


	public OrderDetails updateAddress(OrderDetails orderDetails) {
		OrderDetails orderDetails1 = checkoutRepository.findById(orderDetails.getId());
		System.out.println(orderDetails1);
		orderDetails1.setId(orderDetails.getId());
		orderDetails1.setAddress(orderDetails.getAddress());
		return checkoutRepository.save(orderDetails1);

	}

	public List<OrderDetails> getStatusPending(){
		return checkoutRepository.findByStatus("Pending");
	}

}
