package com.foodDelivery.Services;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.foodDelivery.Entities.Dish;
import com.foodDelivery.Helpers.DishHelper;
import com.foodDelivery.Repository.DishRepository;

@Component
public class DishServices {
    @Autowired
    private DishHelper dishHelper;

    @Autowired
    private DishRepository dishRepository;

    public Dish saveDish(MultipartFile file, Dish dish) throws IOException {
        byte[] data = file.getBytes();
        boolean f = dishHelper.fileUploadHelper(file, data);
        int id=dishRepository.findAll().size()+1;
        int hotelid=1;
        if (f) {
            dish.setId(id);
            dish.setHotelid(hotelid);
            dish.setImage(data);
            dish.setDate(LocalDate.now());
            dish.setRating(2.5);
            System.out.println("File Upload Successfully");
        } else {
            System.out.println("File Upload Failed");
        }
        return dishRepository.save(dish);
    }

    public void deleteDish(int id) {
        dishRepository.deleteById(id);
    }

    public Dish updateDish(Dish dish) {
        return dishRepository.save(dish);
    }

    public List<Dish> getDishies() {
        return dishRepository.findAll();
    }

    public Dish getDish(int id) {
        return dishRepository.findById(id);
    }
}
