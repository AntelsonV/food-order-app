package com.foodDelivery.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;


import com.foodDelivery.Entities.User;

import com.foodDelivery.Repository.UserRepository;

@Service
public class UserServices {
	@Autowired
	UserRepository userRepository;
//	@Autowired
//	public PasswordEncoder passwordEncoder;

	public User saveUser(User user) {
		int id = userRepository.findAll().size();
		user.setId(id + 1);
		return userRepository.save(user);
	}

	public List<User> getAllUser() {
		return userRepository.findAll();
	}
	
	public User getUserById(int id) {
		return userRepository.findById(id);
	}
	
	public User updateUser(User user) {
		return userRepository.save(user);
	}

	public void deleteUser(int id) {
		userRepository.deleteById(id);
	}

	

















//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//		User userDetail = userRepository.findByUsername(username);
//		System.out.println(userDetail.getUsername());
//
//		// Converting userDetail to UserDetails
//		return new UserInfoDetails(userDetail);
//	}
//
//	public String addUser(User user) {
//		user.setPassword(passwordEncoder.encode(user.getPassword()));
//		userRepository.save(user);
//		return "User Added Successfully";
//	}
}
