async function onloadDetails() {
    try {
        let response = await fetch("http://localhost:8080/checkout/status");
        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }
        let orderPending = await response.json();

        console.log(orderPending);

        let mainContainer = document.getElementById("orders");
        orderPending.forEach(async element => {
            console.log(element.dishid);
            let dish=await getdish(element.dishid);
            console.log(dish);

            let subContainer = document.createElement("div");
            subContainer.setAttribute("class", "row m-3");

            let innerContainer = document.createElement("div");
            innerContainer.setAttribute("class", "container");
            innerContainer.innerHTML = `<div class="order-card card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title">User Details</h5>
                                    <p class="card-text"><strong>Name:</strong>
                                        <label id="UserName">${dish.userName}</label>
                                    </p>
                                    <p class="card-text"><strong>Phone Number:</strong>
                                        <label id="UserNumber">${element.phoneNumber}</label>
                                    </p>
                                    <p class="card-text"><strong>Email:</strong>
                                        <label id="UserEmail">${element.email}</label>
                                    </p>
                                    <p class="card-text"><strong>Address:</strong>
                                        <label id="UserAddress">${element.address}</label>
                                    </p>
                                </div>
                                <div class="col">
                                    <h5 class="card-title">Dish Ordered</h5>
                                    <div class="d-flex">
                                        <img src="${element.dishImage}" alt="Dish Image" height="130" id="DishImage">
                                        <div class="ms-5">
                                            <p class="card-text"><strong>Dish Name:</strong>
                                                <label id="DishName">${dish.name}</label>
                                            </p>
                                            <p class="card-text"><strong>Dish Price:</strong>
                                                <label id="DishPrice">${element.dishPrice}</label>
                                            </p>
                                            <p class="card-text"><strong>Quantity:</strong>
                                                <label id="DishQuantity">${element.quantity}</label>
                                            </p>
                                            <p class="card-text"><strong>Total Amount to Pay:</strong>
                                                <label id="TotalAmount">${(element.dishPrice * element.quantity).toFixed(2)}</label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <button class="btn btn-success me-5" onclick="acceptOrder('${element.id}')">Accept Order</button>
                                <button class="btn btn-danger" onclick="cancelOrder('${element.id}')">Cancel Order</button>
                            </div>
                        </div>
                    </div>`;

            subContainer.appendChild(innerContainer);
            mainContainer.appendChild(subContainer);
        });
    } catch (error) {
        console.error("Failed to load order details:", error);
    }
}

// Functions to handle accept and cancel actions
function acceptOrder(orderId) {
    console.log(`Accepting order with ID: ${orderId}`);
    // Implement accept order functionality here
}

function cancelOrder(orderId) {
    console.log(`Canceling order with ID: ${orderId}`);
    // Implement cancel order functionality here
}

async function getdish(id) {
    let getdish=await fetch(`http://localhost:8080/dish/${id}`);
    let dish=await getdish.json();
    
    return dish;
}
