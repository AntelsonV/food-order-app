let price;
let hotelid;
let dishid;
let sumValue;
let itemamount;
let deliverycharges;
let totalgst;
let topayamount;
let address2;
let imagearray;

//Item Price Calculation
// function totalClick(click) {
//     let totalClicks = document.getElementById("totalClicks");
//     let itemprice = document.getElementById("itemprice");

//     let currentTotalClicks = parseInt(totalClicks.innerText);
//     // let itemPriceValue = price;

//     sumValue = currentTotalClicks + click;
//     totalClicks.innerHTML = sumValue;
//     console.log(sumValue + click);

//     let newPrice = price * sumValue;
//     itemprice.innerText = newPrice;
//     itemamount = newPrice;
//     console.log(newPrice);

//     let itemtotal = document.getElementById("itemtotal");
//     itemtotal.innerText = newPrice;

//     let delivery = document.getElementById("delivery");
//     deliverycharges = newPrice * 0.02.toFixed(2);
//     delivery.innerText = deliverycharges;
   

//     let gstcharge = document.getElementById("gstcharge");
//     totalgst = ((parseFloat(newPrice) + parseFloat(deliverycharges)) * 0.09).toFixed(2);
//     gstcharge.innerText = totalgst;
   

//     let topay = document.getElementById("topay");
//     let topayamount = Math.round(parseFloat(newPrice) + parseFloat(deliverycharges) + parseFloat(totalgst)).toFixed(2);
//     topay.innerText = topayamount;
    
//     console.log(topayamount);
//     let x = document.getElementById("hidecart");
//     let y = document.getElementById("visiblecart");
//     let z = document.getElementById("body1");
//     if (sumValue <= 0) {
//         totalClicks.innerText = 0;
//         x.style.display = "none";
//         z.style.backgroundColor = "white";
//         y.style.display = "block";

//     }
// }

function totalClick(dishId, value,basePrice) {
    let qtyElement = document.getElementById(`totalClicks-${dishId}`);
    let priceElement = document.getElementById(`itemprice-${dishId}`);
   

    if (qtyElement && priceElement) {
        let currentQty = parseInt(qtyElement.innerText);
        let newQty = currentQty + value;
        if (newQty >= 0) {
            qtyElement.innerText = newQty;

            // Calculate the new price
            let newPrice = newQty * parseFloat(basePrice);

            // Update the price element
            priceElement.innerText = newPrice.toFixed(2); // Format to 2 decimal places

            itemtotal=parseFloat(basePrice);
        }
    } else {
        console.error(`Elements with IDs totalClicks-${dishId} or itemprice-${dishId} not found.`);
    }
}

// Call the function to fetch and display the data
getOrderDetails();

//Fetch the data
async function getOrderDetails() {
    console.log("Get Data");

    //Get the value of Dish

    let response = await fetch("http://localhost:8080/dish");
    console.log(response);
    let data = await response.json();
    console.log(data);
    console.log(data[0].name);
    // price = data[0].price;
    // dishid = data[0].id;
    // hotelid = data[0].hotelid;
    // sumValue = data[0].price;
    // itemamount = data[0].price;
    const dishDetails = document.getElementById('dishdetails');
    dishDetails.innerHTML = '';

    // Iterate over the data and create elements for each dish
    data.forEach((element) => {
        // Create a new row for each dish
        let dishRow = document.createElement("div");
        dishRow.setAttribute("class", "row mt-4");

        // Create and append dish name element
        let dishname1 = document.createElement("div");
        dishname1.setAttribute("class", "col");
        dishname1.innerHTML = `<p style="font-size: small;" id="dishname-${element.id}" name="dishname">${element.name}</p>`;
        
        dishRow.appendChild(dishname1);

        // Create and append dish quantity controls
        let dishqty = document.createElement("div");
        dishqty.setAttribute("class", "col ms-3");

        let dishqtyBox = document.createElement("div");
        dishqtyBox.setAttribute("class", "box");

        let dishqtyMinus = document.createElement("span");
        dishqtyMinus.setAttribute("class", "symbol minus");
        dishqtyMinus.style.fontWeight = "bold";
        dishqtyMinus.innerText = "-";
        dishqtyMinus.setAttribute("onclick", `totalClick(${element.id}, -1, ${element.price})`);

        let dishqtyNumber = document.createElement("span");
        dishqtyNumber.setAttribute("class", "span number");
        dishqtyNumber.style.fontSize = "small";
        dishqtyNumber.setAttribute("id", `totalClicks-${element.id}`);
        dishqtyNumber.innerText = "1";

        let dishqtyPlus = document.createElement("span");
        dishqtyPlus.setAttribute("class", "symbol plus");
        dishqtyPlus.style.fontWeight = "bold";
        dishqtyPlus.innerText = "+";
        dishqtyPlus.setAttribute("onclick", `totalClick(${element.id}, 1, ${element.price})`);

        dishqtyBox.appendChild(dishqtyMinus);
        dishqtyBox.appendChild(dishqtyNumber);
        dishqtyBox.appendChild(dishqtyPlus);

        dishqty.appendChild(dishqtyBox);
        dishRow.appendChild(dishqty);

        // Create and append dish price element
        let dishprice = document.createElement("div");
        dishprice.setAttribute("class", "col");

        let dishpriceContainer = document.createElement("div");
        dishpriceContainer.setAttribute("class", "ms-1");

        let dishpriceLabel = document.createElement("label");
        dishpriceLabel.style.fontSize = "small";
        dishpriceLabel.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" fill="currentColor" class="bi bi-currency-rupee" viewBox="0 0 16 16">
                                <path d="M4 3.06h2.726c1.22 0 2.12.575 2.325 1.724H4v1.051h5.051C8.855 7.001 8 7.558 6.788 7.558H4v1.317L8.437 14h2.11L6.095 8.884h.855c2.316-.018 3.465-1.476 3.688-3.049H12V4.784h-1.345c-.08-.778-.357-1.335-.793-1.732H12V2H4z"/>
                            </svg>`;

        let dishpriceValue = document.createElement("label");
        dishpriceValue.setAttribute("id", `itemprice-${element.id}`);
        dishpriceValue.innerText = element.price;

        dishpriceContainer.appendChild(dishpriceLabel);
        dishpriceContainer.appendChild(dishpriceValue);
        dishprice.appendChild(dishpriceContainer);

        dishRow.appendChild(dishprice);

        // Append the dish row to the dishdetails div
        dishDetails.appendChild(dishRow);
    });

    console.log("Finished rendering dishes");

    deliverycharges = itemamount * 0.02.toFixed(2);
    totalgst = ((parseFloat(itemamount) + parseFloat(deliverycharges)) * 0.09).toFixed(2);
    topayamount = itemamount + deliverycharges + totalgst;

    document.getElementById('dishname').innerText = data[0].name;
    document.getElementById('itemprice').innerText = data[0].price;
    document.getElementById("itemtotal").innerText = data[0].price;

//Get Image from database

     imagearray= data[0].image;
     let imgelement=document.getElementById("dishimage");
     imgelement.src='data:image/jpeg;base64,' + imagearray;

    const delivery = document.getElementById("delivery");
    deliverycharges = price * 0.02.toFixed(2);
    delivery.innerText = deliverycharges;

    const gstcharge = document.getElementById("gstcharge");
    totalgst = ((parseFloat(price) + parseFloat(deliverycharges)) * 0.09).toFixed(2);
    gstcharge.innerText = totalgst;

    const topay = document.getElementById("topay");
    topayamount = Math.round(parseFloat(price) + parseFloat(deliverycharges) + parseFloat(totalgst)).toFixed(2);
    topay.innerText = topayamount;

    //Get the value of Hotel

    response = await fetch("http://localhost:8080/hotel");
    console.log(response);
    data = await response.json();
    console.log(data);
    console.log(data[0].name);

    document.getElementById('hotelname').innerText = data[0].name;
    document.getElementById('hoteladdress').innerText = data[0].address;

    response = await fetch("http://localhost:8080/checkout");
    console.log(response);
    data = await response.json();
    console.log(data);
    console.log(data[0].name);
    console.log(data.length);
    address2 = data[0].address;
    document.getElementById("addr4").innerText = data[0].address;
    document.getElementById("addr5").innerText = data[data.length - 1].address;
    // document.getElementById("addr6").innerText=data[0].address;
    console.log(data);

}
//Post the data
async function postData() {
    console.log(price + "," + sumValue + "," + itemamount + "," + delfee + "," + gst + "," + grandtotal + "," + address2 + "," + hotelid + "," + dishid);
    if (price > 0 && sumValue > 0 && itemamount > 0 && delfee > 0 && gst > 0 && grandtotal > 0 && address2 != null && hotelid > 0 && dishid > 0) {
        let flag = await fetch("http://localhost:8080/checkout", {
            method: "POST",
            body: JSON.stringify({
                initialprice: price,
                quantity: sumValue,
                itemtotal: itemamount,
                deliveryfee: delfee,
                gst: gst,
                totalamount: grandtotal,
                address: address2,
                hotelid: hotelid,
                dishid: dishid
            }),
            headers: { "Content-type": "application/json;charset=UTF-8" },
        });
        console.log(flag);
        Swal.fire({
            title: "Good job!",
            text: "Order Received Successfully",
            icon: "success"
        });
    } else {
        alert("Fill the page");
    }
}

async function saveAddress() {

    var doorNo = document.getElementById("doorno").value;
    var address1 = document.getElementById("address1").value;
    var landmark = document.getElementById("landmark").value;

    if (doorNo.trim() === "" || address1.trim() === "" || landmark.trim() === "") {

        $('#validationModal').modal('show');
        return;
    }
    // Construct the full address
    var fullAddress = `${doorNo}, ${address1}, ${landmark}`;
    address2 = fullAddress;
    // Set the address to the addr4 label
    document.getElementById("addr4").innerText = fullAddress;

    response = await fetch("http://localhost:8080/checkout");
    data = await response.json();


    document.getElementById("addr5").innerHTML = data[data.length - 1].address;
    // Close the offcanvas
    var offcanvasElement = document.getElementById("address");
    var offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
    offcanvasInstance.hide();
}
function selectAddress() {
    document.getElementById("addr6").innerText = address2;
    console.log(address2);
    document.getElementById("select").style.display = "none";
    document.getElementById("selected").style.display = "block";


}

