let price;
let hotelid;
let dishid;
let sumValue;
let itemamount;
let delfee;
let gst;
let grandtotal;
let address2;
let imagearray;

//Item Price Calculation
function totalClick(click) {
    let totalClicks = document.getElementById("totalClicks");
    let itemprice = document.getElementById("itemprice");

    let currentTotalClicks = parseInt(totalClicks.innerText);
    // let itemPriceValue = price;

    sumValue = currentTotalClicks + click;
    totalClicks.innerHTML = sumValue;
    console.log(sumValue + click);

    let newPrice = price * sumValue;
    itemprice.innerText = newPrice;
    itemamount = newPrice;
    console.log(newPrice);

    let itemtotal = document.getElementById("itemtotal");
    itemtotal.innerText = newPrice;

    let delivery = document.getElementById("delivery");
    let deliverycharges = newPrice * 0.02.toFixed(2);
    delivery.innerText = deliverycharges;
    delfee = deliverycharges;

    let gstcharge = document.getElementById("gstcharge");
    let totalgst = ((parseFloat(newPrice) + parseFloat(deliverycharges)) * 0.09).toFixed(2);
    gstcharge.innerText = totalgst;
    gst = totalgst;

    let topay = document.getElementById("topay");
    let topayamount = Math.round(parseFloat(newPrice) + parseFloat(deliverycharges) + parseFloat(totalgst)).toFixed(2);
    topay.innerText = topayamount;
    grandtotal = topayamount;

    console.log(topayamount);
    let x = document.getElementById("hidecart");
    let y = document.getElementById("visiblecart");
    let z = document.getElementById("body1");
    if (sumValue <= 0) {
        totalClicks.innerText = 0;
        x.style.display = "none";
        z.style.backgroundColor = "white";
        y.style.display = "block";

    }
}

//Fetch the data
async function getOrderDetails() {
    console.log("Get Data");

    //Get the value of Dish

    let response = await fetch("http://localhost:8080/dish");
    console.log(response);
    let data = await response.json();
    console.log(data);
    console.log(data[0].name);
    price = data[0].price;
    dishid = data[0].id;
    hotelid = data[0].hotelid;
    sumValue = data[0].price;
    itemamount = data[0].price;
    delfee = itemamount * 0.02.toFixed(2);
    gst = ((parseFloat(itemamount) + parseFloat(delfee)) * 0.09).toFixed(2);
    grandtotal = itemamount + delfee + gst;
    document.getElementById('dishname').innerText = data[0].name;
    document.getElementById('itemprice').innerText = data[0].price;
    document.getElementById("itemtotal").innerText = data[0].price;
//Get Image from database
     imagearray= data[0].image;
     let imgelement=document.getElementById("dishimage");
     imgelement.src='data:image/jpeg;base64,' + imagearray;

    const delivery = document.getElementById("delivery");
    const deliverycharges = price * 0.02.toFixed(2);
    delivery.innerText = deliverycharges;

    const gstcharge = document.getElementById("gstcharge");
    const totalgst = ((parseFloat(price) + parseFloat(deliverycharges)) * 0.09).toFixed(2);
    gstcharge.innerText = totalgst;

    const topay = document.getElementById("topay");
    const topayamount = Math.round(parseFloat(price) + parseFloat(deliverycharges) + parseFloat(totalgst)).toFixed(2);
    topay.innerText = topayamount;

    //Get the value of Hotel

    response = await fetch("http://localhost:8080/hotel");
    console.log(response);
    data = await response.json();
    console.log(data);
    console.log(data[0].name);

    document.getElementById('hotelname').innerText = data[0].name;
    document.getElementById('hoteladdress').innerText = data[0].address;

    response = await fetch("http://localhost:8080/checkout");
    console.log(response);
    data = await response.json();
    console.log(data);
    console.log(data[0].name);
    console.log(data.length);
    address2 = data[0].address;
    document.getElementById("addr4").innerText = data[0].address;
    document.getElementById("addr5").innerText = data[data.length - 1].address;
    // document.getElementById("addr6").innerText=data[0].address;
    console.log(data);

}
//Post the data
async function postData() {
    console.log(price + "," + sumValue + "," + itemamount + "," + delfee + "," + gst + "," + grandtotal + "," + address2 + "," + hotelid + "," + dishid);
    if (price > 0 && sumValue > 0 && itemamount > 0 && delfee > 0 && gst > 0 && grandtotal > 0 && address2 != null && hotelid > 0 && dishid > 0) {
        let flag = await fetch("http://localhost:8080/checkout", {
            method: "POST",
            body: JSON.stringify({
                initialprice: price,
                quantity: sumValue,
                itemtotal: itemamount,
                deliveryfee: delfee,
                gst: gst,
                totalamount: grandtotal,
                address: address2,
                hotelid: hotelid,
                dishid: dishid
            }),
            headers: { "Content-type": "application/json;charset=UTF-8" },
        });
        console.log(flag);
        Swal.fire({
            title: "Good job!",
            text: "Order Received Successfully",
            icon: "success"
        });
    } else {
        alert("Fill the page");
    }
}

async function saveAddress() {

    var doorNo = document.getElementById("doorno").value;
    var address1 = document.getElementById("address1").value;
    var landmark = document.getElementById("landmark").value;

    if (doorNo.trim() === "" || address1.trim() === "" || landmark.trim() === "") {

        $('#validationModal').modal('show');
        return;
    }
    // Construct the full address
    var fullAddress = `${doorNo}, ${address1}, ${landmark}`;
    address2 = fullAddress;
    // Set the address to the addr4 label
    document.getElementById("addr4").innerText = fullAddress;

    response = await fetch("http://localhost:8080/checkout");
    data = await response.json();


    document.getElementById("addr5").innerHTML = data[data.length - 1].address;
    // Close the offcanvas
    var offcanvasElement = document.getElementById("address");
    var offcanvasInstance = bootstrap.Offcanvas.getInstance(offcanvasElement);
    offcanvasInstance.hide();
}
function selectAddress() {
    document.getElementById("addr6").innerText = address2;
    console.log(address2);
    document.getElementById("select").style.display = "none";
    document.getElementById("selected").style.display = "block";


}

