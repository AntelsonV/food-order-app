package com.tv.AddProduct;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.bson.Document;
import org.bson.types.Binary;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

@MultipartConfig
@WebServlet("/adddish")
public class AddDishes extends HttpServlet {

	public static byte[] uploadimagetolocal(String uploadpath, InputStream is) throws IOException {
		FileOutputStream fos = new FileOutputStream(uploadpath);
		byte[] data = new byte[is.available()];
		is.read(data);
		fos.write(data);
		fos.close();
		return data;

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String title = request.getParameter("title");
		String price = request.getParameter("price");
		String hotel = request.getParameter("hotel");
		Part foodimage = request.getPart("foodimage");
		String rating = request.getParameter("rating");

		String foodimagename = foodimage.getSubmittedFileName();
		// path of local storage and inputstream
		String uplaodpath = "C:/Users/Rashid/git/tech/food-order-app/FoodDeliveryApp/src/main/webapp/images/dishImage/"
				+ foodimagename;
		InputStream inputStream = foodimage.getInputStream();
		byte[] data = uploadimagetolocal(uplaodpath, inputStream);

		// MongoDB connection and add image steps
		MongoClient mongoClient = new MongoClient("localhost", 27017);
		MongoDatabase database = mongoClient.getDatabase("Monicacatering");
		MongoCollection collection = database.getCollection("Dishes");

		Document document = new Document("image", new Binary(data)).append("title", title).append("price", price)
				.append("hotel", hotel).append("rating", rating);
		collection.insertOne(document);

		mongoClient.close();
		response.sendRedirect("AddProduct.jsp");
	}
}
