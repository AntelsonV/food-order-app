package com.tv.AddProduct;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;
import org.bson.types.Binary;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.tv.Entity.Hotel;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@MultipartConfig
@WebServlet("/addhotel")
public class AddHotels extends HttpServlet {
	public static byte[] uploadimagetolocal(String uploadpath, InputStream is) throws IOException {
		FileOutputStream fos = new FileOutputStream(uploadpath);
		byte[] data = new byte[is.available()];
		is.read(data);
		fos.write(data);
		fos.close();
		return data;

	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Hotel hotel = new Hotel();
		hotel.setName(request.getParameter("hotelname"));
		hotel.setAddress(request.getParameter("address"));
		hotel.setHotelImage(request.getPart("hotelimage"));
		hotel.setRating(Integer.parseInt(request.getParameter("rating")));

		String hotelImageName = hotel.getHotelImage().getSubmittedFileName();

		String uplaodpath = "C:/Users/Rashid/git/tech/food-order-app/FoodDeliveryApp/src/main/webapp/images/hotelImage/"
				+ hotelImageName;
		InputStream inputStream = hotel.getHotelImage().getInputStream();
		byte[] data = uploadimagetolocal(uplaodpath, inputStream);
		
		MongoClient mongoClient = new MongoClient("localhost", 27017);
		MongoDatabase database = mongoClient.getDatabase("Monicacatering");
		MongoCollection collection = database.getCollection("Hotels");

		Document document = new Document("image", new Binary(data)).append("name", hotel.getName()).append("address", hotel.getAddress())
				.append("rating", hotel.getRating());
		collection.insertOne(document);
		HttpSession session=request.getSession();
		List<Document> hotels=(List<Document>) session.getAttribute("hotels");
		hotels.add(document);
		session.setAttribute("hotels", hotels);
		mongoClient.close();
		response.sendRedirect("AddProduct.jsp");
	}
}
