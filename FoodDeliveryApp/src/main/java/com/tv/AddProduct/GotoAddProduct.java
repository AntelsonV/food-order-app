package com.tv.AddProduct;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/addproduct")
public class GotoAddProduct extends HttpServlet{
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException {
		MongoClient mongoClient = new MongoClient("localhost", 27017);
		MongoDatabase database = mongoClient.getDatabase("Monicacatering");
		MongoCollection collection = database.getCollection("Hotels");
		FindIterable findIterable=collection.find();
		Iterator iterator=findIterable.iterator();
		List<Document> hotels=new ArrayList<Document>();
		while (iterator.hasNext()) {
			Document document= (Document) iterator.next();
			hotels.add(document);
		}
		HttpSession session=request.getSession();
		session.setAttribute("hotels", hotels);
		response.sendRedirect("AddProduct.jsp");
	}
}
