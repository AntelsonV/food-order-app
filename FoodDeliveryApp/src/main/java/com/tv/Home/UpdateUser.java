package com.tv.Home;

import java.io.IOException;
import java.util.Iterator;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.tv.Entity.User;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/updateuser")
public class UpdateUser extends HttpServlet{
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException {
		User user=new User();
		user.setName(request.getParameter("username"));
		user.setContact(request.getParameter("contact"));
		user.setEmail(request.getParameter("email"));
		user.setPassword(request.getParameter("password"));
		user.setLocation(request.getParameter("location"));
		
		MongoClient mongoclient = new MongoClient("localhost", 27017);
		MongoDatabase db = mongoclient.getDatabase("Monicacatering");
		MongoCollection<Document> collection = db.getCollection("usersignup");

		collection.deleteOne(Filters.eq("email",user.getEmail()));
		FindIterable findIterable = collection.find();
		Iterator iterator = findIterable.iterator();
		boolean flag = true;
		while (iterator.hasNext()) {
			Document document = new Document();
			document = (Document) iterator.next();
			String email2 = document.getString("email");
			System.out.println(user.getEmail()+" "+email2);
			System.out.println(email2.equals(user.getEmail()));
			if (email2.equals(user.getEmail())) {
				flag = false;
				break;
			}
			
		}
		if(flag) {
			
			Document doc = new Document("name", user.getName()).append("contact", user.getContact()).append("email", user.getEmail())
					.append("password", user.getPassword()).append("location", user.getLocation());
			collection.insertOne(doc);
			HttpSession session=request.getSession();
			session.setAttribute("currentloginuser", user);
			response.sendRedirect("profile.jsp");
		}
		else {
			response.sendRedirect("profile.jsp");
		}
	}
}
