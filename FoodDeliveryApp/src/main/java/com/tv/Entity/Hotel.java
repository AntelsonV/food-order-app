package com.tv.Entity;

import jakarta.servlet.http.Part;

public class Hotel {
	private String name;
	private String address;
	private Part hotelImage;
	private Integer rating;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Part getHotelImage() {
		return hotelImage;
	}

	public void setHotelImage(Part hotelImage) {
		this.hotelImage = hotelImage;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "Hotel [name=" + name + ", address=" + address + ", hotelImage=" + hotelImage + ", rating=" + rating
				+ "]";
	}

	public Hotel(String name, String address, Part hotelImage, int rating) {
		super();
		this.name = name;
		this.address = address;
		this.hotelImage = hotelImage;
		this.rating = rating;
	}

	public Hotel() {
		super();
		// TODO Auto-generated constructor stub
	}

}
