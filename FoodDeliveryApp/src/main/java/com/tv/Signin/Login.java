package com.tv.Signin;

import java.io.IOException;
import java.util.Iterator;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.tv.Entity.User;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class Login extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		MongoDbCheck m = new MongoDbCheck();
		if(m.check(email,password))
		{
			HttpSession session=request.getSession();
			MongoClient mongoclient = new MongoClient("localhost", 27017);
			MongoDatabase db = mongoclient.getDatabase("Monicacatering");
			MongoCollection<Document> collection = db.getCollection("usersignup");
			
			FindIterable<Document> findIterable=collection.find();
			Iterator iterator=findIterable.iterator();
			User user=new User();
			while (iterator.hasNext()) {
				Document signup =  (Document) iterator.next();
				String email1=signup.getString("email");
				if(email.equals(email1)) {
					System.out.println(signup);
					 user.setName(signup.getString("name"));
					 user.setContact(signup.getString("contact"));
					 user.setEmail(signup.getString("email"));
					 user.setPassword(signup.getString("password"));
					 user.setLocation(signup.getString("location"));
					 
					 System.out.println(user);
					
				}
				
			}
			session.setAttribute("currentloginuser", user);
			response.sendRedirect("home.jsp");
		}
		else {
			response.sendRedirect("Signin.jsp");
		}
	}

}
