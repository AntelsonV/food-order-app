<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Latest compiled JavaScript -->
   


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css"
        integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poetsen+One&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sedan+SC&display=swap" rel="stylesheet">
    <link rel="icon" href="./images/Signin/Monica_Catering_Icon.png" type="image/x-icon">
    <title>Home Page</title>
    <style>
        a {
            text-decoration: none;
            color: black;
        }

        a:hover {
            color: rgb(39, 190, 47);
        }

        .carousel-caption {
            color: rgb(39, 190, 47);
        }

        .poetsen-one-regular {
            font-family: "Poetsen One", sans-serif;
            font-weight: 500;
            font-style: normal;
        }

        .sedan-sc-regular {
            font-family: "Sedan SC", serif;
            font-weight: 400;
            font-style: normal;
        }

        .catimg {
            border-radius: 50%;
            width: 200px;
        }

        .catimg1 {
            width: 200px;
        }

        
    </style>
</head>

<body class="sedan-sc-regular">
    <div class="container-fluid">
        <div class="row mt-2 sticky-top">
            <div class="col ">
                <nav class="navbar navbar-expand-sm bg-light ms-2" style="border-radius:100px;">
                    <div class="container-fluid">
                        <div class="">
                            <a class="navbar-brand" href="javascript:void(0)"><img
                                    src="./images/Signin/Monica_Catering_Icon.png" alt="App Logo"
                                    style="width: 100px;border-radius: 50%;"></a>
                        </div>
                        <div class="">
                            <ul class="navbar-nav ">
                                <li class="nav-item me-5 ">
                                    <a href=""><i class="fa-solid fa-magnifying-glass me-2"></i>
                                        <label for="">Search</label></a>
                                </li>
                                <li class="nav-item me-5 ">
                                    <a href="tologin"><i class="fa-regular fa-user me-2"></i>
                                        <label for="">Sign In</label></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row justify-content-center text-center w-75" style="margin-left: 10%; ">
            <div id="demo" class="carousel slide text-center " data-bs-ride="carousel">

                <!-- Indicators/dots -->
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="3"></button>
                    <button type="button" data-bs-target="#demo" data-bs-slide-to="4"></button>
                </div>

                <!-- The slideshow/carousel -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="./images/Signin/dish2.jpg" alt="Los Angeles" class="d-block w-100 " style="height: 370px;">
                        <div class="carousel-caption">
                            <i class="fa-solid fa-star"></i>
                            <h3>Los Angeles</h3>
                            <p>We had such a great time in LA!</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="carousel-caption">
                            <i class="fa-solid fa-star"></i>
                            <h3>Los Angeles</h3>
                            <p>We had such a great time in LA!</p>
                        </div>
                        <img src="./images/Signin/dish1.jpg" alt="Los Angeles" class="d-block w-100" style="height: 400px;">

                    </div>
                    <div class="carousel-item">
                        <img src="./images/Signin/dish3.jpg" alt="Los Angeles" class="d-block w-100 " style="height: 400px;">
                        <div class="carousel-caption">
                            <i class="fa-solid fa-star"></i>
                            <h3>Los Angeles</h3>
                            <p>We had such a great time in LA!</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="./images/Signin/dish4.jpg" alt="Los Angeles" class="d-block w-100 " style="height: 400px;">
                        <div class="carousel-caption">
                            <i class="fa-solid fa-star"></i>
                            <h3>Los Angeles</h3>
                            <p>We had such a great time in LA!</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="./images/Signin/dish5.jpg" alt="Los Angeles" class="d-block w-100 " style="height: 400px;">
                        <div class="carousel-caption">
                            <i class="fa-solid fa-star"></i>
                            <h3>Los Angeles</h3>
                            <p>We had such a great time in LA!</p>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls/icons -->
                <button class="carousel-control-prev " type="button" data-bs-target="#demo" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon "></span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </button>
            </div>

        </div>
        <div class="row mt-3 ms-5">
            <div class="container-fluid ">
                <a href="">
                    <label for="" class="poetsen-one-regular" style="font-size: larger;">Categories</label>
                </a>
            </div>
            <div class="row mt-4">
                <div class="col ">
                    <a href="#">
                        <div class="row justify-content-center ">
                            <img src="./images/Signin/dish2.jpg" alt="" class="catimg">
                        </div>
                        <div class="row text-center">
                            <label for="">Dish 1</label>
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="#">
                        <div class="row justify-content-center">
                            <img src="./images/Signin/dish1.jpg" alt="" class="catimg">
                        </div>
                        <div class="row text-center">
                            <label for="">Dish 2</label>
                        </div>
                    </a>
                </div>
                <div class="col ">
                    <a href="">
                        <div class="row justify-content-center">
                            <img src="./images/Signin/dish3.jpg" alt="" class="catimg">
                        </div>
                        <div class="row text-center ">
                            <label for="">Dish 3</label>
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="#">
                        <div class="row justify-content-center">
                            <img src="./images/Signin/dish4.jpg" alt="" class="catimg">
                        </div>
                        <div class="row text-center">
                            <label for="">Dish 4</label>
                        </div>
                    </a>
                </div>
                <div class="col ">
                    <a href="">
                        <div class="row justify-content-center">
                            <img src="./images/Signin/dish5.jpg" alt="" class="catimg">
                        </div>
                        <div class="row text-center">
                            <label for="">Dish 5</label>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row mt-3 ms-5">
            <div class="container-fluid ">
                <a href="">
                    <label for="" class="poetsen-one-regular" style="font-size: larger;"> Top Rated Restaurant </label>
                </a>
            </div>
            <div class="row mt-4">
                <div class="col ">
                    <a href="">
                        <div class="row justify-content-center ">
                            <img src="./images/Signin/Kfc_logo.png" alt="" class="catimg1">
                        </div>
                        <div class="row text-center  ">
                            <label for="">KFC</label>
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="">
                        <div class="row justify-content-center">
                            <img src="./images/Signin/McDonalds-logo.png" alt="" class="catimg1">
                        </div>
                        <div class="row text-center">
                            <label for="">McDonalds</label>
                        </div>
                    </a>
                </div>
                <div class="col ">
                    <a href="">
                        <div class="row justify-content-center">
                            <img src="./images/Signin/Pizza-Hut-logo.png" alt="" class="catimg1">
                        </div>
                        <div class="row text-center ">
                            <label for="">Pizza Hut</label>
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="">
                        <div class="row justify-content-center">
                            <img src="./images/Signin/Starbucks-logo.png" alt="" class="catimg1">
                        </div>
                        <div class="row text-center">
                            <label for="">Starbucks</label>
                        </div>
                    </a>
                </div>
                <div class="col ">
                    <a href="">
                        <div class="row justify-content-center">
                            <img src="./images/Signin/Burger-King_Logo.png" alt="" class="catimg1">
                        </div>
                        <div class="row text-center">
                            <label for="">Burger mKing</label>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div style="height: 300px;">
            <footer>

            </footer>
        </div>
    </div>
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>