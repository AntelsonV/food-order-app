<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Latest compiled JavaScript -->


    <link rel="icon" href="./image/Monica Catering Icon.png" type="image/x-icon">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css"
        integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poetsen+One&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sedan+SC&display=swap" rel="stylesheet">
    <title>Order Categories Page</title>
    <style>
        #myDIV {
            display: grid;
            grid-template-columns: auto auto auto;
            grid-template-rows: auto;
            padding: 10px;
            margin: 3%;
        }

        #myDIV div {
            padding: 1% 0;
            font-size: 20px;
            margin: 1%;
        }

        .image {
            border-radius: 10%;
        }

        #label {
            color: black;
            font-family: Georgia, 'Times New Roman', Times, serif;
        }

        .addtocart {
            background-color: black;
            color: aliceblue;
            width: 40%;
            border-radius: 7px;
        }

        .addtocart:hover {
            background-color: green;
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row mt-2 sticky-top">
            <div class="col ">
                <nav class="navbar navbar-expand-sm bg-light ms-2" style="border-radius:100px;">
                    <div class="container-fluid">
                        <div class="">
                            <a class="navbar-brand" href="javascript:void(0)"><img
                                    src="./image/Monica Catering Icon.png" alt="App Logo"
                                    style="width: 100px;border-radius: 50%;"></a>
                        </div>
                        <div class="">
                            <ul class="navbar-nav ">
                                <li class="nav-item me-5 ">
                                    <a href=""><i class="fa-solid fa-magnifying-glass me-2"></i>
                                        <label for="">Search</label></a>
                                </li>
                                <li class="nav-item me-5 ">
                                    <a href=""><i class="fa-regular fa-user me-2"></i>
                                        <label for="">Sign In</label></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="">
            <div id="myDIV" class="">
                <div class="item1" style="border: 3px solid black; border-radius: 10px;">
                    <div class="row">
                        <img src="./image/dish1.jpg" alt="" class="image">
                    </div>
                    <div class="row">
                        <div class="col-3 text-center">
                            <i class="fa-solid fa-star">4.5</i>
                        </div>
                        <div class="col">
                            <label for="text" id="label">Noodles</label><br>
                            <label for="text" id="label">$120</label><br>
                            <label for="text" id="label" style="font-weight: bold;">Shammizz</label><br>
                            <label for="text" id="label">Truvandrum</label><br>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <input type="button" value="Add To Cart" class="addtocart" >
                    </div>
                </div>
                <div class="item1" style="border: 3px solid black; border-radius: 10px;">
                    <div class="row">
                        <img src="./image/dish1.jpg" alt="" class="image">
                    </div>
                    <div class="row">
                        <div class="col-3 text-center">
                            <i class="fa-solid fa-star">4.5</i>
                        </div>
                        <div class="col">
                            <label for="text" id="label">Noodles</label><br>
                            <label for="text" id="label">$120</label><br>
                            <label for="text" id="label" style="font-weight: bold;">Shammizz</label><br>
                            <label for="text" id="label">Truvandrum</label><br>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <input type="button" value="Add To Cart" class="addtocart" >
                    </div>
                </div>
                <div class="item1" style="border: 3px solid black; border-radius: 10px;">
                    <div class="row">
                        <img src="./image/dish1.jpg" alt="" class="image">
                    </div>
                    <div class="row">
                        <div class="col-3 text-center">
                            <i class="fa-solid fa-star">4.5</i>
                        </div>
                        <div class="col">
                            <label for="text" id="label">Noodles</label><br>
                            <label for="text" id="label">$120</label><br>
                            <label for="text" id="label" style="font-weight: bold;">Shammizz</label><br>
                            <label for="text" id="label">Truvandrum</label><br>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <input type="button" value="Add To Cart" class="addtocart" >
                    </div>
                </div>
                <div class="item1" style="border: 3px solid black; border-radius: 10px;">
                    <div class="row">
                        <img src="./image/dish1.jpg" alt="" class="image">
                    </div>
                    <div class="row">
                        <div class="col-3 text-center">
                            <i class="fa-solid fa-star">4.5</i>
                        </div>
                        <div class="col">
                            <label for="text" id="label">Noodles</label><br>
                            <label for="text" id="label">$120</label><br>
                            <label for="text" id="label" style="font-weight: bold;">Shammizz</label><br>
                            <label for="text" id="label">Truvandrum</label><br>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <input type="button" value="Add To Cart" class="addtocart" >
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>