<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>SignIn-Monica Catering</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

<style type="text/css">
.b1{
    background-color:snow;
}
.h1{
   font-variant: small-caps;
   font-weight: bold;
}
.a1{
    text-decoration: none;
    color:rgb(39, 190, 47);
    font-variant: small-caps;

}
.d1{
    display: flex;
    margin-top: 5%;
    margin-left: 5%;
}
.logodiv{
    padding-left: 220px;
}
.formstyle{
    margin-left: 10%;
    margin-top:0px;
    margin-bottom: 10px;
}
.inputdiv{
    padding-right: 40%;
    padding-top: 10px;
}
.input1{
    border:solid 1px rgb(39, 190, 47);
}
.checkbox{
    padding-right: 20px;
    padding-top: 15px;
    font-size: smaller;
    font-weight: 4px;
    color:rgb(39, 190, 47);
    font-variant: small-caps;
}
.dropdown1{
    top: 40px;
    padding-right: 60px;
    
}
.select{
    padding-right: 46%;
    padding-top: 8px;
    padding-bottom: 8px;
    border-radius: 6px;
}
.button1{
    position: relative;
    top: 30px;
    left: 35px;
    padding: 5px 40px 5px 40px;
    font-variant: small-caps;
    font-size: 22px;
    font-weight: bold; 
    margin-left: 25%;
    background-color: rgb(39, 190, 47);
    border-radius: 20px;
}
.button1:hover{
    background-color: rgb(0, 255, 64);
    color: white;
}
@media only screen and (max-width:500px){
    .logodiv{
        padding-left: 140px;
    }
    .select{
        padding-right: 20%;
    }
}
@media only screen and (max-width:800px){
    .select{
        padding-right: 20%;
        
    }

}
</style>
</head>
<body class="b1">
	<div class="offcanvas offcanvas-end " style="width:40%" id="demo">
		<div class="offcanvas-header">
			<div class="d1">
				<div>
					<h1 class="h1 offcanvas-title">Sign up</h1>
				</div>
				<div class="logodiv">
					<img src="./images/Signin/Monica_Catering_Icon.png" height="100px"
						width="120px" />
				</div>
			</div>
			<button type="button" class="btn-close" data-bs-dismiss="offcanvas"></button>
		</div>
		<div class="offcanvas-body">
			<div class="formstyle">
				<form action="signup" method="post">
					<div class="form-group inputdiv">
						<label for="name"></label> <input type="text"
							class="form-control input1" name="username"
							placeholder="Enter Your Name" pattern="[A-Za-z]+" required>
					</div>
					<div class="form-group inputdiv">
						<label for="contact"></label> <input type="tel"
							class="form-control input1" name="contact"
							placeholder="Enter Your Contact Number" pattern="[0-9]{10}"
							required>
					</div>
					<div class="form-group inputdiv">
						<label for="email"></label> <input type="email"
							class="form-control input1" name="email"
							placeholder="Enter Valid Email"
							pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$" required>
					</div>
					<div class="form-group inputdiv">
						<label for="pwd"></label> <input type="password"
							class="form-control input1" name="password"
							placeholder="Enter Password" required>
					</div>
					<div class="dropdown1">
						<label for="location"></label> <br> <select name="location"
							id="location" class="select input1">
							<option value="Trivandrum" selected>Trivandrum</option>
							<option value="Karamana">Karamana</option>
							<option value="Thampanoor">Thampanoor</option>
							<option value="Nemam">Nemam</option>
						</select>
					</div>
					<div class="checkbox checkbox">
						<label> <input type="checkbox" required>&nbsp;By
							creating an account, I accept the Terms & Conditions & Privacy
							Policy
						</label>
					</div>
					<div class="">
						<button type="submit" class="btn button1">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="container">
		<div>
			<form action="login" method="post" class="mt-5 mb-3"
				style="width: 500px; height: 500px; padding: 50px; margin-left: 350px;">
				<div class="row mb-3 mt-3">
					<div class="col ">
						<h1 class="h1">SignIn</h1>
						<p>
							or <a href="/reg.html" data-bs-toggle="offcanvas"
								data-bs-target="#demo" class="a1">Create an account
						</p>
						</a>
					</div>
					<div class="col">
						<img src="./images/Signin/Monica_Catering_Icon.png"
							style="width: 100px; margin-left: 120px; border-radius: 30px;">
					</div>

				</div>

				<div class="mb-3">
					<label for="Email" class="form-label"></label> <input type="email"
						class="form-control input1" id="email"
						placeholder="Enter Valid Email" name="email"
						pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$" required>
				</div>
				<div class="mb-3">
					<label for="pwd" class="form-label"></label> <input type="password"
						class="form-control input1" id="password"
						placeholder="Enter Password" name="password" required>
				</div>
				<button type="submit" class="btn button1">Submit</button>
			</form>
		</div>
	</div>
</body>
</html>