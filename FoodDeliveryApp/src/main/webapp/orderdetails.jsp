<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Order Details</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <link href="Orderdetails.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid bg-body ">
        <div class="row mt-2 sticky-top">
            <div class="col ">
                <nav class="navbar navbar-expand-sm bg-light ms-2" style="border-radius:100px;">
                    <div class="container-fluid">
                        <div class="">
                            <a class="navbar-brand" href="javascript:void(0)"><img src="./images/Signin/Monica_Catering_Icon.png"
                                    alt="App Logo" style="width: 100px;border-radius: 50%;"></a>
                        </div>
                        <div>
                        <h1>Order Details</h1>
                        </div>
                        <div class="">
                            <ul class="navbar-nav ">
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div style="padding-top: 30px; text-align: center; font-variant: small-caps;">
            
            <div style="padding-top: 30px; background-color:blanchedalmond; width:75%; margin-left: 15%; border-radius: 25px; ">
                <div class="row ">
                    <div class="col-7 mb-4">
                        <img src="./images/dishImage/dish4.jpg" class="rounded-3" height="250px" ;width="250px" />
                    </div>
                    <div class="col-5" style="padding-left: 30px;">
                        <img src="./images/hotelImage/Kfc_logo.png" style="border-radius: 10%"
                            height="80px" ; width="80px" />
                        <div class="mt-3" style="padding-right: 170px;">
                            <label>Burger chicken ordered me</label>
                        </div>
                        <div class="mt-3" style="display: flex;">
                            <div>
                                <label>5</label>
                            </div>
                            <div style="padding-left: 120px;padding-right: 170px;">
                                <label><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-currency-rupee" viewBox="0 0 16 16">
                                        <path
                                            d="M4 3.06h2.726c1.22 0 2.12.575 2.325 1.724H4v1.051h5.051C8.855 7.001 8 7.558 6.788 7.558H4v1.317L8.437 14h2.11L6.095 8.884h.855c2.316-.018 3.465-1.476 3.688-3.049H12V4.784h-1.345c-.08-.778-.357-1.335-.793-1.732H12V2H4z" />
                                    </svg> 500</label>
                            </div>
                        </div>
                        <div class="mt-3" style="padding-right: 170px;">
                            <label>SK Grand</label>
                        </div>
                        <div class="mt-3" style="padding-right: 170px;">
                            <label>Hyderabad</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=" w-50 text-center " style="margin-left: 27%;border: 1px 0px solid black">
            <div>
                <div class="row mt-3 ">
                    <label>Bill Details:</label>
                    <div class="col-6">
                        <label>Total Item</label>
                    </div>
                    <div class="col-6">
                        <label>Total Item</label>
                    </div>
                </div>
                <div class="row mt-3 ">
                    <div class="col-6">
                        <label>GST Value</label>
                    </div>
                    <div class="col-6">
                        <label><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-currency-rupee" viewBox="0 0 16 16">
                                <path
                                    d="M4 3.06h2.726c1.22 0 2.12.575 2.325 1.724H4v1.051h5.051C8.855 7.001 8 7.558 6.788 7.558H4v1.317L8.437 14h2.11L6.095 8.884h.855c2.316-.018 3.465-1.476 3.688-3.049H12V4.784h-1.345c-.08-.778-.357-1.335-.793-1.732H12V2H4z" />
                            </svg> 50</label>
                    </div>
                </div>
                <div class="row mt-3 ">
                    <div class="col-6">
                        <label>Total Amount</label>
                    </div>
                    <div class="col-6">
                        <label><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-currency-rupee" viewBox="0 0 16 16">
                                <path
                                    d="M4 3.06h2.726c1.22 0 2.12.575 2.325 1.724H4v1.051h5.051C8.855 7.001 8 7.558 6.788 7.558H4v1.317L8.437 14h2.11L6.095 8.884h.855c2.316-.018 3.465-1.476 3.688-3.049H12V4.784h-1.345c-.08-.778-.357-1.335-.793-1.732H12V2H4z" />
                            </svg> 550</label>
                    </div>
                </div>
                <button type="submit" class="btn"
                    style="background-color: green;font-variant: small-caps;font-weight: bold;">Place Order</button>
            </div>
        </div>
    </div>
</body>

</html>