<%@page import="org.bson.Document"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="./image/Monica Catering Icon.png"
	type="image/x-icon">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
	integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
	crossorigin="anonymous">
<title>Add Dishes And Hotels Page</title>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
	href="https://fonts.googleapis.com/css2?family=Sedan+SC&display=swap"
	rel="stylesheet">
<style>
* {
	color: rgb(39, 190, 47);
	font-size: large;
}

.sedan-sc-regular {
	font-family: "Sedan SC", serif;
	font-weight: 400;
	font-style: normal;
}

#dishform {
	display: none;
}
</style>
<script>
	function hotelshow() {
		console.log("hiello");
		let hotelform = document.getElementById("hotelform");
		let dishform = document.getElementById("dishform");
		hotelform.style.display = "block";
		dishform.style.display = "none";
	}
	function dishshow() {
		console.log("hii");
		let hotelform = document.getElementById("hotelform");
		let dishform = document.getElementById("dishform");
		hotelform.style.display = "none";
		dishform.style.display = "block";
	}
</script>
</head>

<body>
	<%
	List<Document> hotels =(List<Document>) session.getAttribute("hotels");
	%>
	<div class="container-fluid sedan-sc-regular">
		<div class="container-fluid mt-2 text-center">
			<div class="row sticky-top">
				<div class="col ">
					<nav class="navbar navbar-expand-sm bg-light ms-2"
						style="border-radius: 100px;">
						<div class="container-fluid">
							<div class="">
								<a class="navbar-brand" href="javascript:void(0)"><img
									src="./images/Signin/Monica_Catering_Icon.png" alt="App Logo"
									style="width: 100px; border-radius: 50%;"></a>
							</div>
							<div class="">
								<ul class="navbar-nav ">
									<li class="nav-item me-5 "><a href=""><i
											class="fa-solid fa-magnifying-glass me-2"></i> <label for="">Search</label></a>
									</li>
									<li class="nav-item me-5 "><a href=""><i
											class="fa-regular fa-user me-2"></i> <label for="">Sign
												Out</label></a></li>
								</ul>
							</div>
						</div>
					</nav>
				</div>
			</div>
			<div class="row mt-2">
				<div class="col-3" style="margin-top: auto; margin-bottom: auto;">
					<div class="row mt-5" onclick="hotelshow()" id="hotelhead">
						<label for="" style="font-weight: 1000;">Add Hotels</label>
					</div>

					<div class="row mt-5" id="dishhead" onclick="dishshow()">
						<label for="" style="font-weight: 1000;">Add Dishes</label>
					</div>
				</div>
				<div class="col bg-body-secondary text-center" id="dishform">

					<form action="adddish" method="POST" class="mt-5 mb-3"
						enctype="multipart/form-data">
						<h1>Dish</h1>
						<div class="mb-3 w-100">
							<label for="Email" class="form-label"></label> <input type="text"
								class="form-control input1" id="title"
								placeholder="Enter Dish Title" name="title" required>
						</div>
						<div class="mb-3 w-100">
							<label for="pwd" class="form-label"></label> <input type="number"
								class="form-control input1" id="price" placeholder="Enter Price"
								name="price" required>
						</div>
						<div class="mb-3 w-100">
							<label for="pwd" class="form-label"></label> <select
								class="form-select" id="sel1"  name="hotel">
								<%
								for (Document doc : hotels) {
								%>
								<option><%=doc.getString("name") %></option>
								<%}%>
								
							</select>
						</div>
						<div class="mb-3">
							<label for="formFile" class="form-label"></label> <input
								class="form-control" type="file" id="formFile" name="foodimage">
						</div>
						<div class="mb-3 w-100">
							<label for="pwd" class="form-label"></label> <input type="number"
								class="form-control input1" id="rating" value="0"
								placeholder="Enter Rating" name="rating">
						</div>
						<button type="submit" class="btn btn-success button1">Submit</button>
					</form>
				</div>
				<div class="col bg-body-secondary text-center" id="hotelform">
					<form action="addhotel" method="POST" class="mt-5 mb-3"
						enctype="multipart/form-data">
						<h1>Hotel</h1>
						<div class="mb-3 w-100">
							<label for="Email" class="form-label"></label> <input type="text"
								class="form-control input1" id="name"
								placeholder="Enter Hotel Name" name="hotelname" required>
						</div>
						<div class="mb-3 w-100">
							<label for="pwd" class="form-label"></label> <input type="text"
								class="form-control input1" id="address"
								placeholder="Enter Address" name="address" required>
						</div>
						<div class="mb-3">
							<label for="formFile" class="form-label"></label> <input
								class="form-control" type="file" id="formFile" name="hotelimage">
						</div>
						<div class="mb-3 w-100">
							<label for="pwd" class="form-label"></label> <input type="number"
								class="form-control input1" id="rating" value="1"
								placeholder="Enter Rating" name="rating">
						</div>
						<button type="submit" class="btn btn-success button1">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>