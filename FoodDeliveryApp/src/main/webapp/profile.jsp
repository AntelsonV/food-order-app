<%@page import="com.tv.Entity.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Profile</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<link href="./CSS/profile.css" rel="stylesheet">
</head>
<body>
    <%
	User loginUser = (User) session.getAttribute("currentloginuser");
	%>
    <div class="container-fluid">
        <div class="row mt-2 sticky-top">
            <div class="col ">
                <nav class="navbar navbar-expand-sm bg-light ms-2" style="border-radius:100px;">
                    <div class="container-fluid">
                        <div class="">
                            <a class="navbar-brand" href="javascript:void(0)"><img
                                    src="./images/Signin/Monica_Catering_Icon.png" alt="App Logo"
                                    style="width: 130px;border-radius: 50%;">
                            </a>
                        </div>
                        <div class="">
                            <h1>Profile Information</h1>
                        </div>
                        <div class="">
                            <ul class="navbar-nav ">
                                <li class="nav-item me-5 ">
                                    <a href=""><i class="fa-solid fa-magnifying-glass me-2"></i>
                                        <label for="" class="text-black">Search</label></a>
                                </li>
                                <li class="nav-item me-5 ">
                                    <a href=""><i class="fa-regular fa-user me-2"></i>
                                        <label for="" class="text-black">Sign Out</label></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="offcanvas offcanvas-end" id="demo" style="background-color: snow; width:35%">
        <div class="offcanvas-header">
            <div class="div1">
                <div>
                    <h1 class=" offcanvas-title">Edit Profile</h1>
                </div>
                
            </div>
            <button type="button" class="btn-close ms-5 text-light" data-bs-dismiss="offcanvas"></button>
        </div>
        <div class="offcanvas-body">
            <div class="formstyle ms-5">
                <form action="updateuser" method="POST">
                    <div class="form-group inputstyle">
                        <label for="name"></label> <input type="text" class="form-control formedit" name="username"
                            placeholder="Enter Your Name" pattern="[A-Za-z]+" value=<%=loginUser.getName()%> required>
                    </div>
                    <div class="form-group inputstyle">
                        <label for="contact"></label> <input type="tel" class="form-control formedit" name="contact"
                            placeholder="Enter Your Contact Number" pattern="[0-9]{10}" value=<%=loginUser.getContact()%> required>
                    </div>
                    <div class="form-group inputstyle">
                        <label for="email"></label> <input type="email" class="form-control formedit" name="email"
                            placeholder="Enter Valid Email" pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$" value=<%=loginUser.getEmail()%> required>
                    </div>
                    <div class="form-group inputstyle">
                        <label for="pwd"></label> <input type="password" class="form-control formedit" name="password"
                            placeholder="Enter Password" value=<%=loginUser.getPassword()%> required>
                    </div>
                    <div class="dropdownedit" style="padding-right: 60px; top: 40px">
                        <label for="location"></label> <br> <select name="location" id="location"
                            class="selectedit formedit">
                            <option value="Trivandrum" selected>Trivandrum</option>
                            <option value="Karamana">Karamana</option>
                            <option value="Thampanoor">Thampanoor</option>
                            <option value="Nemam">Nemam</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success mt-5">Update Profile</button>
                </form>
            </div>
        </div>
    </div>
    <div class="container mt-3" style="margin-left: 11%;">
        <div class="formdiv">
            <label class="m-3 fs-5">Name:</label>
            <label class="m-3 fs-5"><%=loginUser.getName()%></label><br>
            <label class="m-3 fs-5">Contact Number:</label>
            <label class="m-3 fs-5"><%=loginUser.getContact()%></label> <br>
            <label class="m-3 fs-5">Email:</label>
            <label class="m-3 fs-5"><%=loginUser.getEmail()%></label><br>
            <label class="m-3 fs-5">Password:</label>
            <label class="m-3 fs-5"><%=loginUser.getPassword()%></label><br>
            <label class="m-3 fs-5">Location:</label>
            <label class="m-3 fs-5"><%=loginUser.getLocation()%></label><br>
            <button data-bs-toggle="offcanvas" data-bs-target="#demo" class="btn btn-success " style="margin-left: 25%; width: 200px">Edit Profile</button>
        </div>
    </div>
</body>

</html>